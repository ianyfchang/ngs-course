# Using circexplorer2 to identify circRNA
## https://circexplorer2.readthedocs.io/en/latest/tutorial/setup/

# required materials

CIRCexplorer2 requires the gene annotation file and the reference genome sequence file to annotate circular RNAs.
    # example
    gtfToGenePred -geneNameAsName2 gencode.v28.annotation.gtf gencode.v28.annotation.gtf2genepred
    awk -F "\t" '{print $1"\t"$0}' gencode.v28.annotation.gtf2genepred > tmp && mv tmp gencode.v28.annotation.gtf2genepred

    ls /home/circRNA/
    
    # output
    Chimeric.out.junction  gencode.v28.annotation.gtf2genepred

    export ref=/home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta
    export genepred=/home/circRNA/gencode.v28.annotation.gtf2genepred

# Mapping

    #example
    STAR --chimSegmentMin 10 --runThreadN 10 --genomeDir hg38_STAR_index --readFilesIn read_1.fastq read_2.fastq

# Parse
    mkdir ~/circRNA/
    cd ~/circRNA/

    CIRCexplorer2 parse -t STAR ~/Chimeric.out.junction -b star_back_spliced_junction.bed

# Annotate
## This step is a clone and integration of CIRCexplorer to make CIRCexplorer2 inherit all the functions from CIRCexplorer. Please see our previous Cell paper for detailed information.

    CIRCexplorer2 annotate --low-confidence -r $genepred -g $ref -b star_back_spliced_junction.bed -o star_circularRNA_known.txt 

# Final output

## output format https://circexplorer2.readthedocs.io/en/latest/modules/annotate/#output

    
#!/bin/bash
# make all programs executable

# add users
useradd -d /home/std01 -s /bin/bash -m std01
useradd -d /home/std02 -s /bin/bash -m std02
useradd -d /home/std03 -s /bin/bash -m std03
useradd -d /home/std04 -s /bin/bash -m std04
useradd -d /home/std05 -s /bin/bash -m std05
useradd -d /home/std06 -s /bin/bash -m std06
useradd -d /home/std07 -s /bin/bash -m std07
useradd -d /home/std08 -s /bin/bash -m std08
useradd -d /home/std09 -s /bin/bash -m std09


chown -R std01:std01 /home/std01
chown -R std02:std02 /home/std02
chown -R std03:std03 /home/std03
chown -R std04:std04 /home/std04
chown -R std05:std05 /home/std05
chown -R std06:std06 /home/std06
chown -R std07:std07 /home/std07
chown -R std08:std08 /home/std08
chown -R std09:std09 /home/std09


echo 'std01:!yOurPassW0rD^' |chpasswd
echo 'std02:!yOurPassW0rD^' |chpasswd
echo 'std03:!yOurPassW0rD^' |chpasswd
echo 'std04:!yOurPassW0rD^' |chpasswd
echo 'std05:!yOurPassW0rD^' |chpasswd
echo 'std06:!yOurPassW0rD^' |chpasswd
echo 'std07:!yOurPassW0rD^' |chpasswd
echo 'std08:!yOurPassW0rD^' |chpasswd
echo 'std09:!yOurPassW0rD^' |chpasswd


#echo 'export PATH=/opt/miniconda/envs/gatk/bin:/gatk:/opt/miniconda/envs/gatk/bin:/opt/miniconda/bin:/usr/local/bin:/usr/bin:/bin:$PATH'
echo 'export PATH=/opt/miniconda/envs/gatk/bin:/gatk:/opt/miniconda/envs/gatk/bin:/opt/miniconda/bin:/usr/local/bin:/usr/bin:/bin:$PATH' >> /home/std01/.bashrc

# Start the first process
mkdir /var/run/sshd

/usr/sbin/sshd -D &
status=$?
if [ $status -ne 0 ]; then
  echo "Failed to start SSHD: $status"
  exit $status
fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container will exit with an error
# if it detects that either of the processes has exited.
# Otherwise it will loop forever, waking up every 60 seconds
  
while /bin/true; do
  ps aux |grep sshd |grep -q -v grep
  PROCESS_1_STATUS=$?
  # If the greps above find anything, they will exit with 0 status
  # If they are not both 0, then something is wrong
  if [ $PROCESS_1_STATUS -ne 0 ]; then
    echo "One of the processes has already exited."
    exit -1
  fi
  sleep 60
done

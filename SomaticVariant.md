

# list folders
## print name of current/working directory
    
    pwd
    
    /home/rstudio

## required resources
    
    ls -ahl /home/gatk_bundle_hg38/
        
    total 20G
    drwxr-xr-x.  4 root root  15K Apr 25 03:12 .
    drwxr-xr-x. 14 root root  13K Apr 25 11:59 ..
    -rw-r--r--.  1 root root  12G Apr 25 02:48 1000G.phase3.integrated.sites_only.no_MATCHED_REV.hg38.vcf
    -rw-r--r--.  1 root root 9.3M Apr 25 02:48 1000G.phase3.integrated.sites_only.no_MATCHED_REV.hg38.vcf.idx
    -rw-r--r--.  1 root root  51M Apr 25 02:48 1000G_omni2.5.hg38.vcf.gz
    -rw-r--r--.  1 root root 1.5M Apr 25 02:48 1000G_omni2.5.hg38.vcf.gz.tbi
    -rw-r--r--.  1 root root 1.8G Apr 25 02:49 1000G_phase1.snps.high_confidence.hg38.vcf.gz
    -rw-r--r--.  1 root root 2.1M Apr 25 02:49 1000G_phase1.snps.high_confidence.hg38.vcf.gz.tbi
    -rw-r--r--.  1 root root 3.0M Apr 25 02:49 Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf
    -rw-r--r--.  1 root root 3.0M Apr 25 02:48 Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz
    -rw-r--r--.  1 root root 412K Apr 25 02:49 Axiom_Exome_Plus.genotypes.all_populations.poly.hg38.vcf.gz.tbi
    -rw-r--r--.  1 root root  11G Apr 25 02:49 Homo_sapiens_assembly38.dbsnp138.vcf
    -rw-r--r--.  1 root root  12M Apr 25 02:48 Homo_sapiens_assembly38.dbsnp138.vcf.idx
    -rw-r--r--.  1 root root 569K Apr 25 02:48 Homo_sapiens_assembly38.dict
    -rw-r--r--.  1 root root 3.1G Apr 25 02:50 Homo_sapiens_assembly38.fasta
    -rw-r--r--.  1 root root 477K Apr 25 02:49 Homo_sapiens_assembly38.fasta.64.alt
    -rw-r--r--.  1 root root  20K Apr 25 02:49 Homo_sapiens_assembly38.fasta.64.amb
    -rw-r--r--.  1 root root 445K Apr 25 02:49 Homo_sapiens_assembly38.fasta.64.ann
    -rw-r--r--.  1 root root 3.0G Apr 25 02:50 Homo_sapiens_assembly38.fasta.64.bwt
    -rw-r--r--.  1 root root 768M Apr 25 02:49 Homo_sapiens_assembly38.fasta.64.pac
    -rw-r--r--.  1 root root 1.5G Apr 25 02:49 Homo_sapiens_assembly38.fasta.64.sa
    -rw-r--r--.  1 root root 158K Apr 25 02:49 Homo_sapiens_assembly38.fasta.fai
    -rw-r--r--.  1 root root  59M Apr 25 02:49 Homo_sapiens_assembly38.known_indels.vcf.gz
    -rw-r--r--.  1 root root 1.5M Apr 25 02:48 Homo_sapiens_assembly38.known_indels.vcf.gz.tbi
    -rw-r--r--.  1 root root  20M Apr 25 02:48 Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
    -rw-r--r--.  1 root root 1.5M Apr 25 02:49 Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi
    drwxr-xr-x.  4 root root  13K Apr 25 03:12 S07604715_SureSelect_Human_All_Exon_V6_COSMIC_r2_hs_hg38
    -rw-r--r--.  1 root root 3.0G Apr 25 02:49 af-only-gnomad.hg38.vcf.gz
    -rw-r--r--.  1 root root 2.4M Apr 25 02:49 af-only-gnomad.hg38.vcf.gz.tbi
    -rw-r--r--.  1 root root 1.6K Apr 25 02:49 creatingSeqSubGroupTSV.py
    -rw-r--r--.  1 root root  60M Apr 25 02:49 hapmap_3.3.hg38.vcf.gz
    -rw-r--r--.  1 root root 1.5M Apr 25 02:49 hapmap_3.3.hg38.vcf.gz.tbi
    -rw-r--r--.  1 root root 4.4M Apr 25 02:49 hg38_HA_gene_snp138.vcf
    -rw-r--r--.  1 root root 3.3G Apr 25 02:49 hg38_dbsnp147_20160527.vcf.gz
    -rw-r--r--.  1 root root 2.4M Apr 25 02:49 hg38_dbsnp147_20160527.vcf.gz.tbi
    -rw-r--r--.  1 root root  457 Apr 25 02:49 scatterIntervalList.py
    drwxr-xr-x. 52 root root  14K Apr 25 02:49 scattered_calling_intervals
    -rw-r--r--.  1 root root  88K Apr 25 02:48 sequence_grouping.txt
    -rw-r--r--.  1 root root  88K Apr 25 02:48 sequence_grouping_with_unmapped.txt
    -rw-r--r--.  1 root root 1.3M Apr 25 02:49 small_exac_common_3.hg38.vcf.gz
    -rw-r--r--.  1 root root 237K Apr 25 02:49 small_exac_common_3.hg38.vcf.gz.tbi
    -rw-r--r--.  1 root root 586K Apr 25 02:49 wgs_calling_regions.hg38.interval_list
    -rw-r--r--.  1 root root  17K Apr 25 02:50 wgs_calling_regions.hg38.interval_list.bed
    -rw-r--r--.  1 root root 569K Apr 25 02:49 wgs_coverage_regions.hg38.interval_list
    -rw-r--r--.  1 root root 583K Apr 25 02:48 wgs_evaluation_regions.hg38.interval_list
    -rw-r--r--.  1 root root 569K Apr 25 02:48 wgs_metrics_intervals.interval_list


## list raw fastq files
    ls -ahl /home/fastq

    total 984M
    drwxr-xr-x.  2 root root  12K Apr 25 13:15 .
    drwxr-xr-x. 14 root root  13K Apr 25 11:59 ..
    -rw-r--r--.  1 root root 156M Apr 25 13:05 D467N.chr22.sort.by.name.bam
    -rw-r--r--.  1 root root 261M Apr 25 13:12 D467N_chr22_R1.fq
    -rw-r--r--.  1 root root 261M Apr 25 13:12 D467N_chr22_R2.fq
    -rw-r--r--.  1 root root 220M Apr 25 13:07 D467T.chr22.sort.by.name.bam
    -rw-r--r--.  1 root root 368M Apr 25 13:16 D467T_chr22_R1.fq
    -rw-r--r--.  1 root root 368M Apr 25 13:16 D467T_chr22_R2.fq

# 01. FASTQ to unmapped BAM
## make unmapped bam folder

    mkdir ~/unmapped_bams

## Normal sample    

    gatk FastqToSam \
    --FASTQ /home/fastq/D467N_chr22_R1.fq \
    --FASTQ2 /home/fastq/D467N_chr22_R2.fq \
    --OUTPUT ~/unmapped_bams/D467N.unmapped.bam \
    --READ_GROUP_NAME D467N_L3 \
    --SAMPLE_NAME D467N \
    --LIBRARY_NAME dnaLib1 \
    --PLATFORM_UNIT PU1 \
    --PLATFORM Illumina \
    --SEQUENCING_CENTER CGMMRC

## Tumor sample

    gatk FastqToSam \
    --FASTQ /home/fastq/D467T_chr22_R1.fq \
    --FASTQ2 /home/fastq/D467T_chr22_R2.fq \
    --OUTPUT ~/unmapped_bams/D467T.unmapped.bam \
    --READ_GROUP_NAME D467T_L2 \
    --SAMPLE_NAME D467T \
    --LIBRARY_NAME dnaLib1 \
    --PLATFORM_UNIT PU1 \
    --PLATFORM Illumina \
    --SEQUENCING_CENTER CGMMRC

# 02. BWA mapping
## make unmerged bam folder

    mkdir ~/unmerged_bams

## Align normal fastq to unmerged bam

    gatk SamToFastq \
    --INPUT ~/unmapped_bams/D467N.unmapped.bam \
    --FASTQ /dev/stdout \
    --INTERLEAVE true \
    --NON_PF true | bwa mem \
    -K 100000000 \
    -v 3 \
    -t 4 \
    -p \
    -Y /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    /dev/stdin - 2> >(tee ~/unmerged_bams/D467N.bwa.stderr.log >&2) | samtools view \
    -1 - > ~/unmerged_bams/D467N.unmerged.bam

## Align tumor fastq to unmerged bam

    gatk SamToFastq \
    --INPUT ~/unmapped_bams/D467T.unmapped.bam \
    --FASTQ /dev/stdout \
    --INTERLEAVE true \
    --NON_PF true | bwa mem \
    -K 100000000 \
    -v 3 \
    -t 10 \
    -p \
    -Y /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    /dev/stdin - 2> >(tee ~/unmerged_bams/D467T.bwa.stderr.log >&2) | samtools view \
    -1 - > ~/unmerged_bams/D467T.unmerged.bam

# 03. Merge BAM alignment
## make aligned unsorted bam folder
    
    mkdir aligned_unsorted_bams

## normal sample

    gatk MergeBamAlignment \
    --VALIDATION_STRINGENCY SILENT \
    --EXPECTED_ORIENTATIONS FR \
    --ATTRIBUTES_TO_RETAIN X0 \
    --ALIGNED_BAM ~/unmerged_bams/D467N.unmerged.bam \
    --UNMAPPED_BAM ~/unmapped_bams/D467N.unmapped.bam \
    --OUTPUT ~/aligned_unsorted_bams/D467N.aligned.unsorted.bam \
    --REFERENCE_SEQUENCE /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    --PAIRED_RUN true \
    --SORT_ORDER unsorted \
    --IS_BISULFITE_SEQUENCE false \
    --ALIGNED_READS_ONLY false \
    --CLIP_ADAPTERS false \
    --MAX_RECORDS_IN_RAM 2000000 \
    --ADD_MATE_CIGAR true \
    --MAX_INSERTIONS_OR_DELETIONS -1 \
    --PRIMARY_ALIGNMENT_STRATEGY MostDistant \
    --PROGRAM_RECORD_ID bwamem \
    --PROGRAM_GROUP_VERSION conda_bwa \
    --PROGRAM_GROUP_COMMAND_LINE 'bwa mem -K 100000000 -p -v 3 -t 16 -Y refseq' \
    --PROGRAM_GROUP_NAME bwamem \
    --UNMAPPED_READ_STRATEGY COPY_TO_TAG \
    --ALIGNER_PROPER_PAIR_FLAGS true \
    --UNMAP_CONTAMINANT_READS true

## tumor sample
    gatk MergeBamAlignment \
    --VALIDATION_STRINGENCY SILENT \
    --EXPECTED_ORIENTATIONS FR \
    --ATTRIBUTES_TO_RETAIN X0 \
    --ALIGNED_BAM ~/unmerged_bams/D467T.unmerged.bam \
    --UNMAPPED_BAM ~/unmapped_bams/D467T.unmapped.bam \
    --OUTPUT ~/aligned_unsorted_bams/D467T.aligned.unsorted.bam \
    --REFERENCE_SEQUENCE /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    --PAIRED_RUN true \
    --SORT_ORDER unsorted \
    --IS_BISULFITE_SEQUENCE false \
    --ALIGNED_READS_ONLY false \
    --CLIP_ADAPTERS false \
    --MAX_RECORDS_IN_RAM 2000000 \
    --ADD_MATE_CIGAR true \
    --MAX_INSERTIONS_OR_DELETIONS -1 \
    --PRIMARY_ALIGNMENT_STRATEGY MostDistant \
    --PROGRAM_RECORD_ID bwamem \
    --PROGRAM_GROUP_VERSION conda_bwa \
    --PROGRAM_GROUP_COMMAND_LINE 'bwa mem -K 100000000 -p -v 3 -t 16 -Y refseq' \
    --PROGRAM_GROUP_NAME bwamem \
    --UNMAPPED_READ_STRATEGY COPY_TO_TAG \
    --ALIGNER_PROPER_PAIR_FLAGS true \
    --UNMAP_CONTAMINANT_READS true

# 04. Mark duplicates

## make aligned unsorted dup marked bam folder

    mkdir aligned_unsorted_dupmarked_bam

## normal sample

    gatk MarkDuplicates \
    --INPUT ~/aligned_unsorted_bams/D467N.aligned.unsorted.bam \
    --OUTPUT ~/aligned_unsorted_dupmarked_bam/D467N.aligned.unsorted.duplicates_marked.bam \
    --VALIDATION_STRINGENCY SILENT \
    --METRICS_FILE ~/aligned_unsorted_dupmarked_bam/D467N.duplicate_metrics \
    --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
    --ASSUME_SORT_ORDER queryname \
    --CREATE_MD5_FILE true

## normal sample

    gatk MarkDuplicates \
    --INPUT ~/aligned_unsorted_bams/D467T.aligned.unsorted.bam \
    --OUTPUT ~/aligned_unsorted_dupmarked_bam/D467T.aligned.unsorted.duplicates_marked.bam \
    --VALIDATION_STRINGENCY SILENT \
    --METRICS_FILE ~/aligned_unsorted_dupmarked_bam/D467T.duplicate_metrics \
    --OPTICAL_DUPLICATE_PIXEL_DISTANCE 2500 \
    --ASSUME_SORT_ORDER queryname \
    --CREATE_MD5_FILE true

# 05. Sort and fix tag
## make sorted bam folder

    mkdir ~/sorted_bams

## normal sample

    gatk SortSam \
    --INPUT ~/aligned_unsorted_dupmarked_bam/D467N.aligned.unsorted.duplicates_marked.bam \
    --OUTPUT /dev/stdout \
    --SORT_ORDER coordinate \
    --CREATE_INDEX false \
    --CREATE_MD5_FILE false \
    --USE_JDK_DEFLATER true \
    --USE_JDK_INFLATER true | gatk SetNmMdAndUqTags \
    --INPUT /dev/stdin \
    --OUTPUT ~/sorted_bams/D467N.aligned.duplicate_marked.sorted.bam \
    --CREATE_INDEX true \
    --CREATE_MD5_FILE true \
    --USE_JDK_DEFLATER true \
    --USE_JDK_INFLATER true \
    --REFERENCE_SEQUENCE /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta

## tumor sample

    gatk SortSam \
    --INPUT ~/aligned_unsorted_dupmarked_bam/D467T.aligned.unsorted.duplicates_marked.bam \
    --OUTPUT /dev/stdout \
    --SORT_ORDER coordinate \
    --CREATE_INDEX false \
    --CREATE_MD5_FILE false \
    --USE_JDK_DEFLATER true \
    --USE_JDK_INFLATER true | gatk SetNmMdAndUqTags \
    --INPUT /dev/stdin \
    --OUTPUT ~/sorted_bams/D467T.aligned.duplicate_marked.sorted.bam \
    --CREATE_INDEX true \
    --CREATE_MD5_FILE true \
    --USE_JDK_DEFLATER true \
    --USE_JDK_INFLATER true \
    --REFERENCE_SEQUENCE /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta


# 06. BQSR
## make BQSR folder

   mkdir ~/bqsr

## 06.01 BQSR
### normal sample

    gatk BaseRecalibrator \
    -R /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -I ~/sorted_bams/D467N.aligned.duplicate_marked.sorted.bam \
    --use-original-qualities true \
    -O ~/bqsr/D467N.aligned.duplicate_marked.sorted.recal_data.csv \
    --known-sites /home/gatk_bundle_hg38/Homo_sapiens_assembly38.dbsnp138.vcf \
    --known-sites /home/gatk_bundle_hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz \
    --known-sites /home/gatk_bundle_hg38/Homo_sapiens_assembly38.known_indels.vcf.gz

### tumor sample

    gatk BaseRecalibrator \
    -R /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -I ~/sorted_bams/D467T.aligned.duplicate_marked.sorted.bam \
    --use-original-qualities true \
    -O ~/bqsr/D467T.aligned.duplicate_marked.sorted.recal_data.csv \
    --known-sites /home/gatk_bundle_hg38/Homo_sapiens_assembly38.dbsnp138.vcf \
    --known-sites /home/gatk_bundle_hg38/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz \
    --known-sites /home/gatk_bundle_hg38/Homo_sapiens_assembly38.known_indels.vcf.gz


## 06.02 Apply BQSR
### mkdir analysis-ready bams
    
    mkdir ~/analysis_ready_bams

### nomal sample

    gatk ApplyBQSR \
    -R /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -I ~/sorted_bams/D467N.aligned.duplicate_marked.sorted.bam \
    -O ~/analysis_ready_bams/D467N.aligned.duplicates_marked.recalibrated.bam \
    -bqsr ~/bqsr/D467N.aligned.duplicate_marked.sorted.recal_data.csv \
    --static-quantized-quals 10 \
    --static-quantized-quals 20 \
    --static-quantized-quals 30 \
    --add-output-sam-program-record \
    --create-output-bam-md5 true \
    --use-original-qualities true

### tumor sample

    gatk ApplyBQSR \
    -R /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -I ~/sorted_bams/D467T.aligned.duplicate_marked.sorted.bam \
    -O ~/analysis_ready_bams/D467T.aligned.duplicates_marked.recalibrated.bam \
    -bqsr ~/bqsr/D467T.aligned.duplicate_marked.sorted.recal_data.csv \
    --static-quantized-quals 10 \
    --static-quantized-quals 20 \
    --static-quantized-quals 30 \
    --add-output-sam-program-record \
    --create-output-bam-md5 true \
    --use-original-qualities true

# 07. Mutect2

## make Mutect2 folder
    mkdir ~/mutect2

## run mutect2
 
    gatk Mutect2 \
    -I ~/analysis_ready_bams/D467T.aligned.duplicates_marked.recalibrated.bam \
    -I ~/analysis_ready_bams/D467N.aligned.duplicates_marked.recalibrated.bam \
    -tumor  D467T \
    -normal D467N \
    --disable-read-filter MateOnSameContigOrNoMappedMateReadFilter \
    --germline-resource /home/gatk_bundle_hg38/af-only-gnomad.hg38.vcf.gz \
    --af-of-alleles-not-in-resource 0.0000025 \
    --ignore-itr-artifacts true \
    -bamout ~/mutect2/D467_tumor_normal_m2.bam \
    -R /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -O ~/mutect2/D467.somatic.m2.vcf.gz \
    -L chr22 \
    --native-pair-hmm-threads 6 \
    --dont-use-soft-clipped-bases true
    
### if analyze all chromosomes

    # -L /home/gatk_bundle_hg38/S07604715_SureSelect_Human_All_Exon_V6_COSMIC_r2_hs_hg38/S07604715_Padded.bed

## run GetPileupSummaries for normal bam

    gatk GetPileupSummaries \
    -I ~/analysis_ready_bams/D467N.aligned.duplicates_marked.recalibrated.bam \
    -L /home/gatk_bundle_hg38/small_exac_common_3.hg38.vcf.gz \
    -V /home/gatk_bundle_hg38/small_exac_common_3.hg38.vcf.gz \
    -O ~/mutect2/D467N.pileups.table

## run GetPileupSummaries for tumor bam

    gatk GetPileupSummaries \
    -I ~/analysis_ready_bams/D467T.aligned.duplicates_marked.recalibrated.bam \
    -L /home/gatk_bundle_hg38/small_exac_common_3.hg38.vcf.gz \
    -V /home/gatk_bundle_hg38/small_exac_common_3.hg38.vcf.gz \
    -O ~/mutect2/D467T.pileups.table

## Calculate contamination

    gatk CalculateContamination \
    -I ~/mutect2/D467T.pileups.table \
    -matched ~/mutect2/D467N.pileups.table \
    --tumor-segmentation ~/mutect2/D467segments.table \
    -O ~/mutect2/D467.contamination.table

## filter VCF

    gatk FilterMutectCalls \
    -R /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -V ~/mutect2/D467.somatic.m2.vcf.gz \
    --contamination-table /home/precomputed/D467.contamination.table \
    -O ~/mutect2/D467.somatic.m2.oncefiltered.vcf.gz

## passed variants
    
    zgrep PASS ~/mutect2/D467.somatic.m2.oncefiltered.vcf.gz|less -S


## How mutect2 works

https://gatkforums.broadinstitute.org/gatk/discussion/11136/how-to-call-somatic-mutations-using-gatk4-mutect2

# Resources
https://bioconductor.org/packages/release/bioc/vignettes/maftools/inst/doc/maftools.html
http://tardis.cgu.edu.tw/maftools.html
http://rnd.cgu.edu.tw/vareporter
https://cancer.sanger.ac.uk/cosmic/signatures
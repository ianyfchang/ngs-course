# Prepare materials 
## source: https://cnvkit.readthedocs.io/en/stable/pipeline.html
## The reference genome
    ls /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta
## The gene annotation table
    # Downloaded from http://hgdownload.soe.ucsc.edu/goldenPath/hg38/database/refFlat.txt.gz
    # check the content of gene annotation table
    column -t /home/gatk_bundle_hg38/refFlat.txt |less -S
    # how to read this table?

## Map sequencing reads to the reference genome
    
    # already done in GATK4 data pre-processing

## target: Prepare a BED file of baited regions for use with CNVkit.
    mkdir ~/cnvkit/

    cnvkit.py target \
    /home/gatk_bundle_hg38/S07604715_SureSelect_Human_All_Exon_V6_COSMIC_r2_hs_hg38/S07604715_Padded.bed \
    --annotate \
    /home/gatk_bundle_hg38/refFlat.txt \
    --split \
    -o ~/cnvkit/targets.bed

    head ~/cnvkit/targets.bed

## access: Calculate the sequence-accessible coordinates in chromosomes from the given reference genome, output as a BED file.

    cnvkit.py access \
    /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -s 10000 \
    -o ~/cnvkit/access-10kb.hg38.bed

## antitarget: generate off-target bins (or background)

    cnvkit.py antitarget \
    ~/cnvkit/targets.bed \
    -g ~/cnvkit/access-10kb.hg38.bed \
    -o ~/cnvkit/antitargets.bed

## autobin: Quickly estimate read counts or depths in a BAM file to estimate reasonable on- and (if relevant) off-target bin sizes.

    cnvkit.py autobin \
    ~/analysis_ready_bams/*.bam \
    -t ~/cnvkit/targets.bed \
    -g ~/cnvkit/access-10kb.hg38.bed

## coverage: Calculate coverage in the given regions from BAM read depths.

    cnvkit.py coverage \
    ~/analysis_ready_bams/D467T.aligned.duplicates_marked.recalibrated.bam \
    ~/cnvkit/targets.bed \
    -o ~/cnvkit/D467T.targetcoverage.cnn

    cnvkit.py coverage \
    ~/analysis_ready_bams/D467T.aligned.duplicates_marked.recalibrated.bam \
    ~/cnvkit/antitargets.bed \
    -o ~/cnvkit/D467T.antitargetcoverage.cnn

    cnvkit.py coverage \
    ~/analysis_ready_bams/D467N.aligned.duplicates_marked.recalibrated.bam \
    ~/cnvkit/targets.bed \
    -o ~/cnvkit/D467N.targetcoverage.cnn

    cnvkit.py coverage \
    ~/analysis_ready_bams/D467N.aligned.duplicates_marked.recalibrated.bam \
    ~/cnvkit/antitargets.bed \
    -o ~/cnvkit/D467N.antitargetcoverage.cnn

## reference: Compile a copy-number reference from the given files or directory (containing normal samples).
    
    cnvkit.py reference \
    ~/cnvkit/D467N*coverage.cnn \
    --fasta /home/gatk_bundle_hg38/Homo_sapiens_assembly38.fasta \
    -o ~/cnvkit/reference.cnn

## fix: Combine the uncorrected target and antitarget coverage tables (.cnn) and correct for biases in regional coverage and GC content, according to the given reference.

    cnvkit.py fix \
    ~/cnvkit/D467T.targetcoverage.cnn \
    ~/cnvkit/D467T.antitargetcoverage.cnn \
    ~/cnvkit/reference.cnn \
    -o ~/cnvkit/D467.cnr

## segment: Infer discrete copy number segments from the given coverage table into segmented log2 ratio estimates

    cnvkit.py segment \
    ~/cnvkit/D467.cnr \
    -o ~/cnvkit/D467.cns

## call: Given segmented log2 ratio estimates (.cns), derive each segment’s absolute integer copy numbe

    cnvkit.py call \
    ~/cnvkit/D467.cns \
    -o ~/cnvkit/D467.call.cns


## plot: 
    
    # online examples
    https://cnvkit.readthedocs.io/en/stable/plots.html

## extented reading:

    # whole genome structural variant detection
    
    https://bioinformaticsdotca.github.io/CSHL_2019_Module5_lab
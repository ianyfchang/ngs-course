# docker
## Installation (已經安裝好了，不用再裝)

    https://docs.docker.com/v17.12/install/

    apt-get update && apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && apt-key fingerprint 0EBFCD88

    add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
    
apt-get update && apt-get -y install docker-ce docker-ce-cli containerd.io

# Pull an image from a registry
    docker pull broadinstitute/gatk:latest

    #output
    latest: Pulling from broadinstitute/gatk
    ae79f2514705: Pull complete 
    5ad56d5fc149: Pull complete 
    170e558760e8: Pull complete 
    395460e233f5: Pull complete 
    6f01dc62e444: Pull complete 
    b48fdadebab0: Pull complete 
    6fa8a6238280: Pull complete 
    Digest: sha256:c0f45677e9af6bba65e7234c33a7447f615febd1217e80ea2231fab69bb350a0
    Status: Downloaded newer image for broadinstitute/gatk:latest

# list available docker images
    docker images

    #output
    REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
    broadinstitute/gatk   latest              9e737a9f562c        2 weeks ago         3.84GB

# create a Dockerfile

    mkdir ~/docker-test
    cd ~/docker-test

    vim Dockerfile # use any editor you familar with
    # add following contents

    FROM broadinstitute/gatk:latest

    RUN conda install -y -c conda-forge -c bioconda bwa samtools vim cnvkit circexplorer2

# build your docker image

    docker build -t std[xx]-gatk .    

    docker images

    # output
    REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
    std01-gatk            latest              c99e347d0828        10 minutes ago      6.04GB
    broadinstitute/gatk   latest              9e737a9f562c        2 weeks ago         3.84GB

# Go into the docker you build

    # this step might very slow because of we are using docker in docker mode
    # you can try this on your own computer
    docker run --rm -it -v /home:/home std[xx]-gatk
    
## you will see

    (gatk) root@889eae1f1a0d:/gatk# 

## list mounted volume /home

    ls /home
    ls /home/std01

## try gatk --list

    gatk --list

## you have root privilege to change everythin of the system if you mount system folder like /etc
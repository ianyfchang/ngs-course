FROM broadinstitute/gatk:latest

RUN apt-get update && apt-get -y install \
    openssh-server \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && apt-key fingerprint 0EBFCD88

RUN add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
RUN apt-get update && apt-get -y install docker-ce docker-ce-cli containerd.io

RUN conda install -y -c conda-forge -c bioconda bwa samtools vim cnvkit circexplorer2

EXPOSE 22

COPY ./config.sh /

USER root

CMD ["sh", "/config.sh"]